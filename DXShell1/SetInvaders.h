/***********************************************************************************************************************
* FILE : 		SetInvaders.h
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Abr-12

* DESCRIPTION:
	This file will keep the system general information for this solution.
	Constant declaration.

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
************************************************************************************************************************/
#pragma once




#define WIN_WIDTH						1024
#define WIN_HEIGHT						768
#define ENEMYSHIP_DISPLAY_WIN_WIDTH		960		//Real boundaries of screen based on the Enemyship size. 
#define ENEMYSHIP_DISPLAY_WIN_HEIGHT	704		//Real boundaries of screen based on the Enemyship size. 
#define MAXSQUARESSCREEN				10		//Information base to create a grid 10 x 10
#define PLANETQTT						3
#define ACTIONRANGE						3.5
#define COLLISIONTOTALTIME				1
#define PERC_50							.5
#define PERC_65							.65
#define PERC_30							.3
#define MAXENIMYASSET					3
#define DSATQTT							3
#define MINENIMYASSET					0
#define PI								3.141592653589793238462643383279502884197169399375105820974944592307816406286 

