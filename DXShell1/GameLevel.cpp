/***********************************************************************************************************************
* FILE : 		GameLevel.cpp
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Apr-12

* DESCRIPTION/Requirements:
Set graphics stuff to be used for the GameLevel

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
************************************************************************************************************************/

#include "GameLevel.h"

Graphics* GameLevel::gfx;

