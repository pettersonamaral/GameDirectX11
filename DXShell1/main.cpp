/***********************************************************************************************************************
* FILE : 		main.cpp
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral 
* FIRST VERSION:2018-Feb-11

Introduction
This document specifies the functional, non-functional, and deliverable requirements for the SET Invaders multimedia 
software development assignment. In effect, this assignment leads to the next two assignments in which you will be 
building up a Win32/C++/DirectX 11 application. Meeting the functional requirements helps you learn the deeper 
workings of Graphics, Animation and Sound.


* DESCRIPTION/Requirements:
Build a Chat system that will allow two people to talk to one another on two different computers.
The communication between the two people is managed by a separate program we will call the Chat Server.

Here are the details:
- 2.1	View Background
- 2.2	Place Planets
- 2.3	Draw Placeholder for Place Enemy Ship
- 2.4	Move the Enemy Ship
- 2.5	Resize the Application Window

* Refence:
	Requirements web Page:
		https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/4026375/View


************************************************************************************************************************/



#include <Windows.h>
#include "Graphics.h"
#include "Level1.h"
#include "GameController.h"
#include "SetInvaders.h"
#include "Grid.h"


Graphics* graphics;

/*
Your Mission: Document the following code. What do all these components do?
*/
LRESULT CALLBACK WindowProc(
	HWND hwnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam )
{
	if (uMsg == WM_DESTROY) { PostQuitMessage(0); return 0; }
	
//BIG CHANGE! We are not Drawing/rendering here! 
// We've moved this down to the message handling loop below

	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}


// Have you seen a 'main' for a Win32 app before? Please comment this.
int WINAPI wWinMain(
	HINSTANCE hInstance,
	HINSTANCE prevInstance,
	LPWSTR cmd,
	int nCmdShow
)
{
	//Do a bit of reading - What is this Window Class used for? 
	// What are the major parameters below?
	WNDCLASSEX windowclass;
	ZeroMemory(&windowclass, sizeof(WNDCLASSEX));
	windowclass.cbSize = sizeof(WNDCLASSEX);
	windowclass.hbrBackground = (HBRUSH)COLOR_WINDOW;
	windowclass.hInstance = hInstance;
	windowclass.lpfnWndProc = WindowProc;
	windowclass.lpszClassName = "MainWindow";
	windowclass.style = CS_HREDRAW | CS_VREDRAW; //Alert - This is useful here... what does it do?

	RegisterClassEx(&windowclass);


	RECT rect = { 0, 0, WIN_WIDTH, WIN_HEIGHT };//Do these numbers look significant to you? What are they?
	AdjustWindowRectEx(&rect, WS_OVERLAPPED, false, WS_EX_OVERLAPPEDWINDOW);

	//Below is another important process to understand... what are we doing?
	//Why is this connected to rect we just defined above?
	HWND windowhandle = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW, "MainWindow", "GAS-AS01_Pamaral9857", WS_OVERLAPPEDWINDOW, 100, 100, 
		rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, hInstance, 0);
	if (!windowhandle) return -1;

	graphics = new Graphics();//Start all graphic variables
	if (!graphics->Init(windowhandle))
	{
		delete graphics;
		return -1;
	}

	Grid::DrawGrid(&rect);
	GameLevel::Init(graphics);//instancia o obj graphic
	ShowWindow(windowhandle, nCmdShow);
	GameController::LoadInitialLevel(new Level1()); //Setup level 1 

#pragma region GameLoop
	//Below, we have essentially an infinite loop that will keep the window running and will dispatch/show messages
	//As many people will tell you, most Windows you see are just infinite loops waiting for some kind of work-flow or 
	//system-based interuption.

	//Note - Our message handling has changed from the first demo code.
	//Here, we use a PeekMessage to avoid locking the graphics/windoProc
	//when a message has to be dispatched.
	MSG message;
	message.message = WM_NULL; //Do not have this set to WM_QUIT, which has a specific context
	while (message.message != WM_QUIT)
	{
		if (PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
			//This allows us to send a message to the WindowProc IF there is one
			DispatchMessage(&message);
		else
		{
			//Create a imaginary grid to do positioning the objects
			Grid::DrawGrid(&rect);

			//Update Routine... we've moved the code for handling updates to GameController
			GameController::Update();//Step1

			//Render Routine... This is very modular. GameController now handles the rendering
			graphics->BeginDraw();//Step2 - init all graphics elements
			GameController::Render();//step3 - gamecontroler render the selected level
			graphics->EndDraw();//Step4 - destroy the screen to create again 
		}
	}

#pragma endregion
	delete graphics;
	return 0;
}