/***********************************************************************************************************************
* FILE : 		SpriteSheet.cpp
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Apr-12

Introduction
	All methods to create, execute effects and draw the images for the game.

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
************************************************************************************************************************/

#include "SpriteSheet.h"

/****************************************************
The concept behind this class is that it will be passed
a filename and graphics object/rendertarget, then, will
proceed to create the needed WIC components to read, 
decode, and then encode the bitmap file from disk into
a compatible D2D bitmap. 

We need this approach to be able to address pretty much
any bitmap from disk/resources into the game and use it
within Directx (D2D specifically for now)

*******************************************************/


SpriteSheet::SpriteSheet(wchar_t* filename, Graphics* gfx)
{
	this->gfx = gfx; //save the gfx parameter for later
	bmp = NULL; //This needs to be NULL to start off
	HRESULT hr;

	//Step 1: Create a WIC Factory
	IWICImagingFactory *wicFactory = NULL;
	hr = CoCreateInstance(
		CLSID_WICImagingFactory, //CLS ID of the object about to be made
		NULL, //not needed here, but just denotes this isn't part of an aggregate
		CLSCTX_INPROC_SERVER, //Indicates this DLL runs in the same process
		IID_IWICImagingFactory, //Reference to an Interface that talks to the object
		(LPVOID*)&wicFactory); //This is our pointer to the WICFactory, once set up.

//Step 2: Create a Decoder to read file into a WIC Bitmap
	IWICBitmapDecoder *wicDecoder = NULL;
	hr = wicFactory->CreateDecoderFromFilename(
		filename, //The filename we passed in already
		NULL, //This can be used to indicate other/preferred decoders. Not something we need.
		GENERIC_READ, //indicates we're reading from the file, vs writing, etc.
		WICDecodeMetadataCacheOnLoad, //Needed, but would only help if we were keeping this in WIC
		&wicDecoder); //Our pointer to the Decoder we've setup

//Step 3: Read a 'frame'. We're really just moving the whole image into a frame here
	IWICBitmapFrameDecode* wicFrame = NULL;
	hr = wicDecoder->GetFrame(0, &wicFrame); //0 here means the first frame... or only one in our case
	//Now, we've got a WICBitmap... we want it to be a D2D bitmap

//Step 4: Create a WIC Converter
	IWICFormatConverter *wicConverter = NULL;
	hr = wicFactory->CreateFormatConverter(&wicConverter);

//Step 5: Configure the Converter
	hr = wicConverter->Initialize(
		wicFrame, //Our frame from above
		GUID_WICPixelFormat32bppPBGRA, //Pixelformat //SUPER POWER FULL PART 
		WICBitmapDitherTypeNone, //not important for us here
		NULL, //indicates no palette is needed, not important here
		0.0, //Alpha Transparency, can look at this later
		WICBitmapPaletteTypeCustom //Not important for us here
		);

//Step 6: Create the D2D Bitmap! Finally!
	gfx->GetDeviceContext()->CreateBitmapFromWicBitmap(
		wicConverter, //Our friend the converter
		NULL, //Can specify D2D1_Bitmap_Properties here, not needed now
		&bmp //Our destination bmp we specified earlier in the header
	);
	
	//Let us do some private object cleanup!
	if (wicFactory) wicFactory->Release();
	if (wicDecoder) wicDecoder->Release();
	if (wicConverter) wicConverter->Release();
	if (wicFrame) wicFrame->Release();
}


SpriteSheet::~SpriteSheet()
{
	if (bmp) bmp->Release();
}


//Draw the background
void SpriteSheet::Draw()
{
	gfx->GetDeviceContext()->DrawBitmap(
		bmp, //Bitmap we built from WIC
		D2D1::RectF(0.0f, 0.0f,
			bmp->GetSize().width, bmp->GetSize().height), //Destination rectangle
		1.0f, //Opacity or Alpha
		D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
		//Above - the interpolation mode to use if this object is 'stretched' or 'shrunk'. 
		//Refer back to lecture notes on image/bitmap files
		D2D1::RectF(0.0f, 0.0f, bmp->GetSize().width, bmp->GetSize().height) //Source Rect
		);
}


//======================================================================================
// FUNCTION     : DrawEnemyShip(float x, float y)
// DESCRIPTION  : This function Draw the EnemyShip. 
// PARAMETERS   : float x: X-axis position; float y: Y-axis position
// RETURNS      : void
//======================================================================================
void SpriteSheet::DrawEnemyShip(float x, float y)
{
	try
	{
		float myX = x;											//Get the X axis, value (position), from 0 to 1024
		float myY = y;											//Get the Y axis, value (position), from 0 to 768
		float width = myX + (Grid::GetBlockSizeW() * PERC_50);	//Get 50% width size of a grid square, then sum with X variable. 
		float height = myY + (Grid::GetBlockSizeH() * PERC_50);	//Get 50% height size of a grid square, then sum with Y variable. 

		gfx->GetDeviceContext()->DrawBitmap(
			bmp, //Bitmap we built from WIC
			D2D1::RectF(myX, myY, width, height), //Destination rectangle
			0.9f, //Opacity or Alpha
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			//Above - the interpolation mode to use if this object is 'stretched' or 'shrunk'. 
			//Refer back to lecture notes on image/bitmap files
			D2D1::RectF(0.0f, 0.0f, bmp->GetSize().width, bmp->GetSize().height) //Source Rect
		);
	}
	catch (const std::exception& e)
	{ // caught by reference to base
		std::cout << " a standard exception was caught, with message '"
			<< e.what() << "'\n";
	}
}


//======================================================================================
// FUNCTION     : DrawPlanet()
// DESCRIPTION  : This function setup and draw a planet. 
// PARAMETERS   : int gradePosition = the planet place
// RETURNS      : void
//======================================================================================
void SpriteSheet::DrawPlanet(int x)
{
	try
	{
		float X = Grid::GetXC(x);
		float Y = Grid::GetYC(x);
		float width = Grid::GetXC(x) + (Grid::GetWidthBlock(x) * PERC_50);
		float height = Grid::GetYC(x) + (Grid::GetHeightBlock(x) * PERC_50);



		gfx->GetDeviceContext()->DrawBitmap(
			bmp, //Bitmap we built from WIC
			D2D1::RectF(X, Y, width, height), //Destination rectangle
			0.9f, //Opacity or Alpha
			D2D1_BITMAP_INTERPOLATION_MODE::D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
			//Above - the interpolation mode to use if this object is 'stretched' or 'shrunk'. 
			//Refer back to lecture notes on image/bitmap files
			D2D1::RectF(0.0f, 0.0f, bmp->GetSize().width, bmp->GetSize().height) //Source Rect
		);
	}
	catch (const std::exception& e)
	{ // caught by reference to base
		std::cout << " a standard exception was caught, with message '"
			<< e.what() << "'\n";
	}
}


//======================================================================================
// FUNCTION     : ChromaPlanet
// DESCRIPTION  : This function setup and draw a planet. 
// PARAMETERS   : int gradePosition = the planet place
// RETURNS      : void
// https://msdn.microsoft.com/en-us/library/windows/desktop/dn890715(v=vs.85).aspx#sample_code
//======================================================================================
void SpriteSheet::ChromaPlanet(int gradePosition)
{
	D2D1_SIZE_U size = bmp->GetPixelSize();
	float X = Grid::GetX(gradePosition);
	float Y = Grid::GetY(gradePosition);
	float width = Grid::GetWidthBlock(gradePosition);
	float height = Grid::GetHeightBlock(gradePosition);
	float scaleWidth = (width*.5) / size.width;
	float scaleHeight = (height*.5) / size.height;


	D2D1_RECT_F planetRect;
	planetRect.top = 0;
	planetRect.left = 0;
	planetRect.right = width;
	planetRect.bottom = height; //hight
	


	D2D1_VECTOR_3F Color = { 0.0f, 1.0f, 0.0f };
	gfx->GetDeviceContext()->CreateEffect(CLSID_D2D1ChromaKey, &chromakeyEffect);

	chromakeyEffect->SetInput(0, bmp);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_COLOR, Color);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_TOLERANCE, 0.8f);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_INVERT_ALPHA, false);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_FEATHER, false);


	//Scale Effect
	gfx->GetDeviceContext()->CreateEffect(CLSID_D2D1Scale, &scaleEffect);

	scaleEffect->SetInputEffect(0, chromakeyEffect.Get());
	scaleEffect->SetValue(D2D1_SCALE_PROP_SCALE, D2D1::Vector2F(scaleWidth, scaleHeight));

	gfx->GetDeviceContext()->DrawImage(scaleEffect.Get(), D2D1::Point2F(X, Y), planetRect);

}


//======================================================================================
// FUNCTION     : ChromaExplosionDSat
// DESCRIPTION  : This function setup and draw a Explosion. 
// PARAMETERS   : int gradePosition = the Explosion place
// RETURNS      : void
// https://msdn.microsoft.com/en-us/library/windows/desktop/dn890715(v=vs.85).aspx#sample_code
//======================================================================================
void SpriteSheet::ChromaExplosionDSat(int gradePosition)
{
	D2D1_SIZE_U size = bmp->GetPixelSize();
	float X = Grid::GetX(gradePosition) - 30;
	float Y = Grid::GetY(gradePosition) - 20;
	float width = Grid::GetWidthBlock(gradePosition);
	float height = Grid::GetHeightBlock(gradePosition);
	float scaleWidth = PERC_30;
	float scaleHeight = PERC_30;

	D2D1_RECT_F explosion1Rect;
	explosion1Rect.top = 0;
	explosion1Rect.left = 0;
	explosion1Rect.right = width;
	explosion1Rect.bottom = height; //hight



	D2D1_VECTOR_3F Color = { 0.0f, 1.0f, 0.0f };
	gfx->GetDeviceContext()->CreateEffect(CLSID_D2D1ChromaKey, &chromakeyEffect);

	chromakeyEffect->SetInput(0, bmp);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_COLOR, Color);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_TOLERANCE, 0.8f);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_INVERT_ALPHA, false);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_FEATHER, false);


	//Scale Effect
	gfx->GetDeviceContext()->CreateEffect(CLSID_D2D1Scale, &scaleEffect);

	scaleEffect->SetInputEffect(0, chromakeyEffect.Get());
	scaleEffect->SetValue(D2D1_SCALE_PROP_SCALE, D2D1::Vector2F(0.3, 0.3));

	gfx->GetDeviceContext()->DrawImage(scaleEffect.Get(), D2D1::Point2F(X, Y), explosion1Rect);

}


//======================================================================================
// FUNCTION     : ChromaDSat
// DESCRIPTION  : This function setup and draw a DSat. 
// PARAMETERS   : int gradePosition = the planet place
// RETURNS      : void
// https://msdn.microsoft.com/en-us/library/windows/desktop/dn890715(v=vs.85).aspx#sample_code
//======================================================================================
void SpriteSheet::ChromaDSat(int gradePosition, float angle)
{
	D2D1_SIZE_U size = bmp->GetPixelSize();
	float X = Grid::GetX(gradePosition);
	float Y = Grid::GetY(gradePosition);
	float width = Grid::GetWidthBlock(gradePosition);
	float height = Grid::GetHeightBlock(gradePosition);
	float scaleWidth = (width*.5) / size.width;
	float scaleHeight = (height*.5) / size.height;


	//Set up the rectangle
	D2D1_RECT_F rectDSat;
	rectDSat.top = 0;
	rectDSat.left = 0;
	rectDSat.right = width;
	rectDSat.bottom = height; 


	//Set up the center of the DSat
	D2D1_POINT_2F centerDSat = D2D1::Point2F((size.width*.25), (size.height*.25));


	//Set up the chromaKey effect
	D2D1_VECTOR_3F Color = { 0.0f, 1.0f, 0.0f };
	gfx->GetDeviceContext()->CreateEffect(CLSID_D2D1ChromaKey, &chromakeyEffect);

	chromakeyEffect->SetInput(0, bmp);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_COLOR, Color);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_TOLERANCE, 0.8f);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_INVERT_ALPHA, false);
	chromakeyEffect->SetValue(D2D1_CHROMAKEY_PROP_FEATHER, false);


	//Set up the scale effect
	gfx->GetDeviceContext()->CreateEffect(CLSID_D2D1Scale, &scaleEffect);

	scaleEffect->SetInputEffect(0, chromakeyEffect.Get());
	scaleEffect->SetValue(D2D1_SCALE_PROP_SCALE, D2D1::Vector2F(scaleWidth, scaleHeight));


	//Set up the rotation effect
	gfx->GetDeviceContext()->CreateEffect(CLSID_D2D12DAffineTransform, &rotateEffect);
	rotateEffect->SetInputEffect(0, scaleEffect.Get());

	D2D1_MATRIX_3X2_F matrix = D2D1::Matrix3x2F::Rotation(angle, centerDSat);  

	rotateEffect->SetValue(D2D1_2DAFFINETRANSFORM_PROP_TRANSFORM_MATRIX, matrix);


	//Draw the image at the position
	gfx->GetDeviceContext()->DrawImage(rotateEffect.Get(), D2D1::Point2F(X, Y), rectDSat);
}