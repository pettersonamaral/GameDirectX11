/***********************************************************************************************************************
* FILE : 		Level1.cpp
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Feb-11

Introduction
	This file contains all the logic of the level1 game, how the ship will move, how the planets will be presented and 
	any other requirement necessary for this particular level. 

	Member/attributes are initialized here. 
	
Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
************************************************************************************************************************/
#include "GameController.h"
#include "Graphics.h"
#include "Level1.h"
#include "SetInvaders.h"
#include "Grid.h"

void Level1::Load() //load whole display 
{
	y = ySpeed = 0.1f;	
	enemyEffectNew = 0;													//Variable to control the Enemy Ship asset "rotation" toggle
	yEnemyShip = 0.0;													//Variable to control the Enemy Ship Y-axis
	xEnemyShip = 0.0;													//Variable to control the Enemy Ship X-axis
	lastYaxisValue = 0.0;												//Variable to control the Enemy Ship Y-axis before start down to next row
	enemyShipSpeed = 3.2;												//Variable to control the Enemy Ship speed
	delayAssetEnemyShip = 0;
	enemyAssetToggleDirection = true;									//true means left to right (->), false means right to left (<-)
	isDetectedCollision = false;
	positionCollision = 0;
	durationCollision = COLLISIONTOTALTIME;
	enemyShipMoveDirection = true;										//true means left to right (->), false means right to left (<-)
	Background01 = new SpriteSheet(L".\\img\\Background01.bmp", gfx);	//This is where we specify our file system object!
	
	Planet01 = new SpriteSheet(L".\\img\\Planet1.bmp", gfx);			//This is where we specify our planet object!
	Planet02 = new SpriteSheet(L".\\img\\Planet2.bmp", gfx);			//This is where we specify our planet object!
	Planet03 = new SpriteSheet(L".\\img\\Planet3.bmp", gfx);			//This is where we specify our planet object!
	
	EnemyAsset01 = new SpriteSheet(L".\\img\\A2En1.png", gfx);			//This is where we specify our Enemy Ship object!
	EnemyAsset02 = new SpriteSheet(L".\\img\\A2En2.png", gfx);			//This is where we specify our Enemy Ship object!
	EnemyAsset03 = new SpriteSheet(L".\\img\\A2En3.png", gfx);			//This is where we specify our Enemy Ship object!
	EnemyAsset04 = new SpriteSheet(L".\\img\\A2En4.png", gfx);			//This is where we specify our Enemy Ship object!
	EnemyShip = { EnemyAsset01 ,EnemyAsset02, EnemyAsset03, EnemyAsset04 };//Loading the vector. This is where we specify our Array Enemy Ship object!

	DSat = new SpriteSheet(L".\\img\\DSAT.bmp", gfx);					//This is where we specify our Enemy Ship object!
	ExplosionDSat = new SpriteSheet(L".\\img\\explosion.bmp", gfx);		//This is where we specify our ExplosionDSat object!
	
	InitPlanets(PLANETQTT);												//Init Dsats
	SetPlanetsPositions(PLANETQTT);										//Set Planets position
	InitDSats(DSATQTT);													//Init Dsats
	SetDSatPosition(DSATQTT);											//Set Dsat position
	mciSendString("Stop background.mp3", 0, 0, 0);						//Once the properties window is open, on the left open Configuration Properties->Linker select Input and then enter winmm.lib into Additonal Dependencies.	
	mciSendString("Play background.mp3 repeat", 0, 0, 0);					
}


void Level1::Unload()
{
	delete Background01;
}

//All variable that have to change, happen here
void Level1::Update()
{
	isDetectedCollision = false;
	xSpeed += 0.04f;
	SetDSatRotation(DSATQTT);
	if (xSpeed > WIN_WIDTH)
	{
		xSpeed = 0.0f;									//Reset the X velue 
	}

	delayAssetEnemyShip += 1;
	if (delayAssetEnemyShip == 10)						//Just a delay between the asset changes on Enemyship
	{
		SetEnemyShipToggleAsset();
		delayAssetEnemyShip = 0;
	}

	ySpeed += 0.04f;
	y += ySpeed;
	if (y > WIN_HEIGHT)
	{
		y = 0.0f;
		ySpeed = 0.0f;
	}

	// --- Collision Code ---
	if (GetDSatCollision(DSATQTT))
	{
		durationCollision = 0.0f;
		mciSendString("Stop background.mp3", 0, 0, 0);		
		mciSendString("Play explosion.mp3", 0, 0, 0);
		mciSendString("Play background.mp3 repeat", 0, 0, 0);
	}
	if (durationCollision < COLLISIONTOTALTIME)
	{
		isDetectedCollision = true;
		durationCollision += 0.075f;
		yEnemyShip = 0.0;								//Variable to control the Enemy Ship Y-axis
		xEnemyShip = 0.0;								//Variable to control the Enemy Ship X-axis
	}
	// --- Collision Code ---
	if (isDetectedCollision == false)
	{
		SetEnemyShipNewPosition();
	}
}

//Display the changes/Display the things
void Level1::Render()
{
	gfx->ClearScreen(0.0f, 0.0f, 0.5f);
	Background01->Draw();													//Display Background
	Planet01->ChromaPlanet(planetsPositions[0]);							//Display Planet01
	Planet02->ChromaPlanet(planetsPositions[1]);							//Display Planet02
	Planet03->ChromaPlanet(planetsPositions[2]);							//Display Planet03	
	
	if (!isDetectedCollision)
	{
		EnemyShip[enemyEffectNew]->DrawEnemyShip(xEnemyShip, yEnemyShip);	//Display the EnemyShip	
	}
	
	for (int i = 0; i < DSATQTT; i++)										//Before drawing a new DSat, make sure it is alive
	{
		if (myDSat[i].isDSatLive)
		{
			DSat->ChromaDSat(myDSat[i].gridPositionDSat, myDSat[i].rotateAngleDSat);//Display DSats
		}
	}
	
	if (isDetectedCollision)
	{ 
		ExplosionDSat->ChromaExplosionDSat(positionCollision);
	}
}

//======================================================================================
// FUNCTION     : SetPlanetsPositions(int qtt)
// DESCRIPTION  : This function figure out position to planets. 
// INPUT		: How many planets are there.
// PARAMETERS   : int qtt, position for how many planets
// RETURNS      : void
//======================================================================================
void Level1::SetPlanetsPositions(int qtt)
{
	bool goodToGo = true;
	//Initialize list

	do
	{
		goodToGo = true;
		//Draw a number for each planets
		for (int i = 0; i < qtt; ++i)
		{
			//Get the planet position with number 50% of the maximum of the grid
			planetsPositions[i] = GetRandomGeneration(((MAXSQUARESSCREEN*MAXSQUARESSCREEN)*.5));
			//Sum %40 on the number sorted.
			planetsPositions[i] += (((MAXSQUARESSCREEN*MAXSQUARESSCREEN)*.4));
		}

		for (int i = 0; i < qtt; ++i)
		{
			//I don't wanna >= 90 
			if (planetsPositions[i] >= 90) { goodToGo = false; continue; }

			//Compare all planets spots, they must be different.
			for (int j = 0; j < qtt; ++j)
			{
				if (i != j)
				{
					if (planetsPositions[i] == planetsPositions[j])
					{
						goodToGo = false; continue;
					}
				}
			}
		}
	} while (goodToGo == false);
}


//======================================================================================
// FUNCTION     : InitPlanets(int qtty)
// DESCRIPTION  : This method iniciality the planets variable. 
// PARAMETERS   : int: qtt - Quantity of planets
// RETURNS      : void
//======================================================================================
void Level1::InitPlanets(int qtt)
{
	bool goodToGo = true;
	planetsPositions.clear();
	//Initialize list
	for (int i = 0; i < qtt; ++i) { planetsPositions.push_back(0); }
}


//======================================================================================
// FUNCTION     : SetEnemyShipToggleAsset(void)
// DESCRIPTION  : This function updates the value of the Enemy Asset. By updating the 
//				  variables: (int)enemyEffectNew and (bool)enemyAssetToggleDirection
// PARAMETERS   : void
// RETURNS      : void
//======================================================================================
void Level1::SetEnemyShipToggleAsset(void)
{
	if (enemyAssetToggleDirection == true)			// The movement is happen in this   --->   diretion
	{
		if (enemyEffectNew == MAXENIMYASSET)
		{
			enemyAssetToggleDirection = false;
			enemyEffectNew -= 1;
		}
		else if (enemyEffectNew < MAXENIMYASSET)
		{
			enemyEffectNew += 1;
		}
	}
	else										// The movement is happen in this   <---   diretion
	{
		if (enemyEffectNew == MINENIMYASSET)
		{
			enemyAssetToggleDirection = true;
			enemyEffectNew += 1;
		}
		else if (enemyEffectNew > MINENIMYASSET)
		{
			enemyEffectNew -= 1;
		}
	}
}


//======================================================================================
// FUNCTION     : SetEnemyShipNewPosition(void)
// DESCRIPTION  : This function updates the value of the Enemy movement. By updating the 
//				  variables: (float)yEnemyShip, (float)yEnemyShip and 
//							 (bool)enemyShipMoveDirection
// PARAMETERS   : void
// RETURNS      : void
//======================================================================================
void Level1::SetEnemyShipNewPosition(void)
{
	if (enemyShipMoveDirection == true)							// The movement is happen in this   --->   diretion
	{
		if (xEnemyShip >= ENEMYSHIP_DISPLAY_WIN_WIDTH)			//Achieved the right screen boundaries --->|
		{
			yEnemyShip += enemyShipSpeed;
			if (yEnemyShip >= (lastYaxisValue + Grid::GetBlockSizeH()))
			{
				if (yEnemyShip >= ENEMYSHIP_DISPLAY_WIN_HEIGHT)
				{
					yEnemyShip = 0;								//Achieved the bottom boundary of the screen boundaries 
					Load();
				}
				enemyShipMoveDirection = false;					//inverts bool that controls the direction
				lastYaxisValue = yEnemyShip;					//Update last Y value with new Row position
				xEnemyShip -= enemyShipSpeed;					//decreases X-axis value				
			}
		}
		else if (xEnemyShip < ENEMYSHIP_DISPLAY_WIN_WIDTH)		//Keeps runing until Achieve the right screen boundaries
		{
			xEnemyShip += enemyShipSpeed;						//increases X-axis value
		}
	}
	else														// The movement is happen in this   <---   diretion
	{
		if (xEnemyShip <= MINENIMYASSET)						//Achieved the left screen boundaries |<---
		{
			yEnemyShip += enemyShipSpeed;
			if (yEnemyShip >= (lastYaxisValue + Grid::GetBlockSizeH()))
			{

				if (yEnemyShip >= ENEMYSHIP_DISPLAY_WIN_HEIGHT)
				{
					yEnemyShip = 0;								//Achieved the bottom boundary of the screen boundaries 
					Load();
				}
				enemyShipMoveDirection = true;					//inverts bool that controls the direction
				lastYaxisValue = yEnemyShip;					//Update last Y value with new Row position
				xEnemyShip += enemyShipSpeed;					//increases X-axis value
			}
		}
		else if (xEnemyShip > MINENIMYASSET)					//Keeps runing until Achieve the left screen boundaries
		{
			xEnemyShip -= enemyShipSpeed;						//decreases X-axis value
		}
	}
}


//======================================================================================
// FUNCTION     : InitDSats(int qtty)
// DESCRIPTION  : This method iniciality the DSat variable. 
// PARAMETERS   : int: qtt - Quantity of DSats
// RETURNS      : void
//======================================================================================
void Level1::InitDSats(int qtt)
{
	bool goodToGo = true;

	//Clear the list 
	myDSat.clear();
	//Initialize list
	for (int i = 0; i < qtt; ++i)
	{
		sDSat temp;
		temp.gridPositionDSat = 0.0;												//float rotateAngleDSat;
		temp.rotateAngleDSat = 0;													//int gridPositionDSat;	//Grib positin of the DSat
		temp.isDSatLive = true;
		myDSat.push_back(temp);
	}
}


//======================================================================================
// FUNCTION     : SetDSatPosition(void)
// DESCRIPTION  : This function figure out position to Enemyship. 
// INPUT		: void
// PARAMETERS   : void
// RETURNS      : void
//======================================================================================
void Level1::SetDSatPosition(int qtt)
{
	bool goodToGo = true;


	do
	{
		goodToGo = true;

		//Draw a number for each DSats
		for (int i = 0; i < qtt; ++i)
		{
			//Get the DSats position with number 50% of the maximum of the grid
			myDSat[i].gridPositionDSat = GetRandomGeneration(((MAXSQUARESSCREEN*MAXSQUARESSCREEN)*.5));

			//Sum %40 on the number sorted.
			myDSat[i].gridPositionDSat += (((MAXSQUARESSCREEN*MAXSQUARESSCREEN)*.4));
		}

		for (int i = 0; i < qtt; ++i)
		{
			//I don't wanna >= 90 
			if (myDSat[i].gridPositionDSat >= 90) { goodToGo = false; continue; }

			//Compare all DSats spots, they must be different.
			for (int j = 0; j < qtt; ++j)
			{
				if (i != j)
				{
					if (myDSat[i].gridPositionDSat == myDSat[j].gridPositionDSat)
					{
						goodToGo = false; continue;
					}
				}
			}
		}
	} while (goodToGo == false);
}


//======================================================================================
// FUNCTION     : SetDSatRotation(void)
// DESCRIPTION  : This function figure DSat. 
// INPUT		: void
// PARAMETERS   : void
// RETURNS      : void
//======================================================================================
void Level1::SetDSatRotation(int qtt)
{
	for (int i = 0; i < qtt; i++)
	{
		int rowDSat = myDSat[i].gridPositionDSat / MAXSQUARESSCREEN;
		int rowEnemyShip = Grid::GetRow(yEnemyShip);
		float distance = Grid::GetBlockSizeH();

		if ((abs(rowDSat) - abs(rowEnemyShip)) < ACTIONRANGE)
		{
			//Set up DSat current position
			float xDSat = Grid::GetX(myDSat[i].gridPositionDSat);
			float yDSat = Grid::GetY(myDSat[i].gridPositionDSat);

			//Set rotarion to DSat current position
			float Y = yDSat - yEnemyShip;
			float X = xDSat - xEnemyShip;

			myDSat[i].rotateAngleDSat = (((atan2(Y, X)) *(180 / PI)) - 90);
		}
		else { myDSat[i].rotateAngleDSat = 0; }
	}
}


//======================================================================================
// FUNCTION     : GetDSatCollision(int qtt)
// DESCRIPTION  : This method dectect DSat collision.
// INPUT		: void
// PARAMETERS   : void
// RETURNS      : bool = True means that a collisio happened
//======================================================================================
bool Level1::GetDSatCollision(int qtt)
{
	bool setMadeSuccess = false;
	bool isDSatLive = false;
	for (int i = 0; i < qtt; i++)
	{
		int rowDSat = myDSat[i].gridPositionDSat / MAXSQUARESSCREEN;
		int rowEnemyShip = Grid::GetRow(yEnemyShip);
		int posXCDSat = (int)Grid::GetXC(myDSat[i].gridPositionDSat);
		int posEnemyShip = Grid::GetEnemyShipPosition(xEnemyShip, yEnemyShip);
		int posXEnemyShip = Grid::GetEnemyShipX(posEnemyShip, xEnemyShip);

		if ((myDSat[i].isDSatLive == true) &&
			(rowDSat == rowEnemyShip) && (
			(posXEnemyShip == posXCDSat) ||
			((posXEnemyShip - 1) == posXCDSat) ||
			((posXEnemyShip + 1) == posXCDSat)))			//If EnemyShip and DSat have the same row
		{
			positionCollision = myDSat[i].gridPositionDSat;
			myDSat[i].isDSatLive = false;
			setMadeSuccess = true;
		}
		if (myDSat[i].isDSatLive == true)
		{
			isDSatLive = true;
		}
	}
	return setMadeSuccess;
}


//======================================================================================
// FUNCTION     : GetRandomGeneration
// DESCRIPTION  : This function create a random number
// PARAMETERS   : int limit = the limite of a random number
// RETURNS      : int returnCode			: return the random number.
// http://www.cplusplus.com/reference/cstdlib/rand/
//======================================================================================
int Level1::GetRandomGeneration(int limit)
{
	try
	{
		rand();
		int i = 0;
		int aNumber = (rand() % limit) + 1;
		return aNumber;
	}
	catch (const std::exception& e)
	{
		std::cout << " a standard exception was caught, with message '"
			<< e.what() << "'\n";
	}
}
