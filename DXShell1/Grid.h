/***********************************************************************************************************************
* FILE : 		Grid.h
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Feb-13

* DESCRIPTION:	
	This class will keep the grid information/definition. 
	Holds the Grid functions/methods prototypes, and member/attributes declaration.

* source: 
Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
						and	 http://programming2dgames.com/
************************************************************************************************************************/

#pragma once

#include "SetInvaders.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <Windows.h>


struct SpriteData
{
	float       width;      // width of sprite in pixels
	float       height;     // height of sprite in pixels
	float       x;          // screen location vertical axis   (0 is top left corner of sprite)
	float       yc;         // central screen location
	float       y;			// screen location horizontal axis (0 is top left corner of sprite)
	float       xc;			// Central location of each y spot
	float       scale;      // <1 smaller, >1 bigger
	float       angle;      // rotation angle in radians

};


static SpriteData myGrid
[MAXSQUARESSCREEN*MAXSQUARESSCREEN];

static int windowSizeW;		//Current window size widht
static int windowSizeH;		//Current window size height



class Grid
{


public:
	Grid();
	~Grid();

	static void DrawGrid(RECT* rect);

	static int GetWindowSizeH();
	static int GetWindowSizeW();
	static int GetRow(float yValue);
	static float GetX(int pos);
	static float GetY(int pos);
	static float GetXC(int pos);
	static float GetYC(int pos);
	static float GetWidthBlock(int pos);
	static float GetHeightBlock(int pos);
	static float GetBlockSizeH(void);
	static float GetBlockSizeW(void);	
	static void SetX(int pos, float theValue);
	static void SetY(int pos, float theValue);
	static void SetXC(int pos, float theValue);
	static void SetYC(int pos, float theValue);
	static void SetWindowSizeH(int newValue);
	static void SetWindowSizeW(int newValue);
	static void SetWidth(int pos, float theValue);
	static void SetHeight(int pos, float theValue);
	static int GetEnemyShipPosition(float x, float y);
	static float GetEnemyShipX(int pos, float xEnemyShip);
};