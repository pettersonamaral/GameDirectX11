/***********************************************************************************************************************
* FILE : 		Graphics.cpp
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Apr-12

* DESCRIPTION/Requirements:
This file will keep the DirectX11 2D graphical stuff

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
// https://katyscode.wordpress.com/2013/01/23/migrating-existing-direct2d-applications-to-use-direct2d-1-1-functionality-in-windows-7/
************************************************************************************************************************/

#include "Graphics.h"


/***********************************************************************************
The intent of the Graphics class is to handle our DirectX calls, and to be largely responsible 
for managing the rendertarget.
******************************************************************************************/
//Constructor for Graphics class
Graphics::Graphics()
{
	//factory = NULL;
	Screen = NULL;
	brush = NULL;
	pSink = NULL;
	Direct2D = NULL;
	m_pPathGeometry = NULL;
	Direct2DDevice = NULL;
	Direct3DContext = NULL;
	Direct3DDevice = NULL;
	Direct2DBackBuffer = NULL;
	DXGISwapChain = NULL;
}

//Destructor for Graphics class
//Note that all COM objects we instantiate should be 'released' here 
//Look for comments on COM usage in the corresponding header file.
Graphics::~Graphics()
{
	if (Direct2D) Direct2D->Release();
	if (Screen) Screen->Release();
	if (brush) brush->Release();
	if (m_pPathGeometry) m_pPathGeometry->Release();
	if (pSink) pSink->Release();
	if (Direct2DBackBuffer) Direct2DBackBuffer->Release();
	if (DXGISwapChain) DXGISwapChain->Release();
	if (Direct2DDevice) Direct2DDevice->Release();
	if (Direct3DContext) Direct3DContext->Release();
	if (Direct3DDevice) Direct3DDevice->Release();
}


//======================================================================================
// FUNCTION     : Init
// DESCRIPTION: I've to created a new factory based on Katy web page to graphical 
//              effects because I need to use "ID2D1DeviceContext" instead 
//              "ID2D1HwndRenderTarget"
// PARAMETERS   : int planetPosition = the planet place
// RETURNS      : void
// https://katyscode.wordpress.com/2013/01/23/migrating-existing-direct2d-applications-to-use-direct2d-1-1-functionality-in-windows-7/
//======================================================================================
bool Graphics::Init(HWND windowHandle)
{
	D2D1_FACTORY_OPTIONS options;
	ZeroMemory(&options, sizeof(D2D1_FACTORY_OPTIONS));


	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory1), &options, reinterpret_cast<void **>(&Direct2D));


	//
	// 1. Set the Direct3D feature levels supported by our application :
	// Set feature levels supported by our application
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1
	};

	
	//
	//2. Create the Direct3D device and device context, making sure we set all flags correctly:
	// This flag adds support for surfaces with a different color channel ordering
	// than the API default. It is required for compatibility with Direct2D.
	UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

	// Create Direct3D device and context
	ID3D11Device *device;
	ID3D11DeviceContext *context;
	D3D_FEATURE_LEVEL returnedFeatureLevel;

	D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, 0, creationFlags, featureLevels, ARRAYSIZE(featureLevels), D3D11_SDK_VERSION,
		&device, &returnedFeatureLevel, &context);


	//
	//3. Fetch the underlying interfaces and store them :
	// Fetch the underlying interfaces and store them
	device->QueryInterface(__uuidof(ID3D11Device1), (void **)&Direct3DDevice);
	context->QueryInterface(__uuidof(ID3D11DeviceContext1), (void **)&Direct3DContext);


	//
	//4. Get the underlying DXGI device of the Direct3D device(we�ll need this temporarily 
	// to set up interoperability with Direct2D, see above) :
	IDXGIDevice *dxgiDevice;
	Direct3DDevice->QueryInterface(__uuidof(IDXGIDevice), (void **)&dxgiDevice);


	//
	//5. Create the Direct2D device from the DXGI device:
	Direct2D->CreateDevice(dxgiDevice, &Direct2DDevice);


	//
	//6. Create the Direct2D device context (to be used as the rendering interface later) from the Direct2D device:
	Direct2DDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &Screen);


	//
	//7. The first step is to get a DXGI factory object (which lets us create a swap chain) for the default graphics adapter:
	// Get the GPU we are using
	IDXGIAdapter *dxgiAdapter;
	dxgiDevice->GetAdapter(&dxgiAdapter);

	// Get the DXGI factory instance
	IDXGIFactory2 *dxgiFactory;
	dxgiAdapter->GetParent(IID_PPV_ARGS(&dxgiFactory));

	//
	//8. Then we describe the swap chain:
	// Describe Windows 7-compatible Windowed swap chain
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = { 0 };

	swapChainDesc.Width = 0;
	swapChainDesc.Height = 0;
	swapChainDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swapChainDesc.Stereo = false;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 2;
	swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = 0;


	//
	//9. Create the swap chain and target it at the application window
	// Create DXGI swap chain targeting a window handle (the only Windows 7-compatible option)
	dxgiFactory->CreateSwapChainForHwnd(Direct3DDevice, windowHandle, &swapChainDesc, nullptr, nullptr, &DXGISwapChain);


	//
	//10. We now need to get the swap chain�s back buffer (the off-screen rendering buffer) in a format that we can 
	// link to Direct2D to make it the render target. This format is an IDXGISurface:
	// Get the back buffer as an IDXGISurface (Direct2D doesn't accept an ID3D11Texture2D directly as a render target)
	IDXGISurface *dxgiBackBuffer;
	DXGISwapChain->GetBuffer(0, IID_PPV_ARGS(&dxgiBackBuffer));


	//
	//11. We�ll create a bitmap with Direct2D which has the properties needed to be a render target, and link it to this back buffer:
	// Get screen DPI
	FLOAT dpiX, dpiY;
	Direct2D->GetDesktopDpi(&dpiX, &dpiY);

	// Create a Direct2D surface (bitmap) linked to the Direct3D texture back buffer via the DXGI back buffer
	D2D1_BITMAP_PROPERTIES1 bitmapProperties =
		D2D1::BitmapProperties1(D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
		D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE), dpiX, dpiY);

	Screen->CreateBitmapFromDxgiSurface(dxgiBackBuffer, &bitmapProperties, &Direct2DBackBuffer);


	//
	//12. Finally, set the bitmap as the Direct2D device context�s render target:
	// Set surface as render target in Direct2D device context
	Screen->SetTarget(Direct2DBackBuffer);


	//
	//13. Don�t forget to release all the temporary stuff from the device-dependent and window-size-dependent 
	// sections before the functions close:
	if (dxgiBackBuffer) dxgiBackBuffer->Release();
	if (dxgiFactory) dxgiFactory->Release();
	if (dxgiAdapter) dxgiAdapter->Release();
	if (dxgiDevice) dxgiDevice->Release();
	if (context) context->Release();
	if (device) device->Release();


	return true;
}


void Graphics::ClearScreen(float r, float g, float b) 
{
	Screen->Clear(D2D1::ColorF(r, g, b));
}
