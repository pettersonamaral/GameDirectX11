/***********************************************************************************************************************
* FILE : 		Grid.cpp
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Feb-13

* DESCRIPTION/Requirements:
This file will keep the imaginary grid to keeping the things happens on this game, and all setters and getters methods.

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
************************************************************************************************************************/

#include "Grid.h"


Grid::Grid()
{
	SpriteData mySpriteData = {
		0,		//int         width;      // width of sprite in pixels
		0,		//int         height;     // height of sprite in pixels
		0.0,	//float       x;          // screen location vertical axis   (0 is top left corner of sprite)
		0.0,	//float       c;          // central screen location
		0.0,	//float       y;		  // screen location horizontal axis (0 is top left corner of sprite)
		0.0,	//float       scale;      // <1 smaller, >1 bigger
		0.0,	//float       angle;      // rotation angle in radians
	};

}

Grid::~Grid()
{

}


//======================================================================================
// FUNCTION     : DrawGrid
// DESCRIPTION  : This function create the imaginary grid
// PARAMETERS   : RECT* rec : the configuration of the main window
// RETURNS      : void
//======================================================================================
void Grid::DrawGrid(RECT* rect)
{
	try
	{
		float posX = 0.0, posY = 0.0;
		float blockSizeH = 0;
		float blockSizeW = 0;

		SetWindowSizeH(rect->bottom - rect->top);
		SetWindowSizeW(rect->right - rect->left);

		blockSizeH = GetBlockSizeH();
		blockSizeW = GetBlockSizeW();

		for (int i = 0; i < (MAXSQUARESSCREEN*MAXSQUARESSCREEN); i++)
		{
			if (i != 0)
			{
				if ((i % MAXSQUARESSCREEN) != 0) //if it is NOT == 0
				{
					posX += blockSizeW;

					SetY(i, posY);
					SetX(i, posX);
					SetYC(i, (((blockSizeH / 2) + posY)));
					SetXC(i, ((blockSizeW / 2) + posX));
					SetWidth(i, blockSizeW);
					SetHeight(i, blockSizeH);
				}
				else // if % == 0 (first columns)
				{
					posY += blockSizeH;
					posX = 0;

					SetX(i, 0.0);
					SetY(i, posY);
					SetYC(i, (((blockSizeH / 2) + posY)));
					SetXC(i, ((blockSizeW / 2) + posX));
					SetWidth(i, blockSizeW);
					SetHeight(i, blockSizeH);
				}
			}
			else
			{
				//If i==0
				SetY(i, 0.0);
				SetX(i, 0.0);
				SetYC(i, ((blockSizeH)/2) );
				SetXC(i, (blockSizeW / 2) );
				SetWidth(i, blockSizeW);
				SetHeight(i, blockSizeH);
			}
		}
	}
	catch (const std::exception& e)
	{ 
		std::cout << " a standard exception was caught, with message '"
			<< e.what() << "'\n";
	}
}


//======================================================================================
// FUNCTION     : GetEnemyShipPosition
// DESCRIPTION  : Creates a new Y position to EnemyShip
// PARAMETERS   : void
// RETURNS      : int returnCode			: return the position of emenypostion.
//======================================================================================
int Grid::GetEnemyShipPosition(float x, float y)
{
	if (x < 0)
	{
		x = 0.0;
	}
	float blockSizeH = GetBlockSizeH();
	float blockSizeW = GetBlockSizeW();
	int xPos = (int)x;
	int yPos = (int)y;
	int gridPosition = 0;

	if (yPos == 0)
	{
		gridPosition = (int)x / (int)blockSizeW;
	}
	else if (xPos == 0)
	{
		gridPosition = (int)y / (int)blockSizeH;
	}
	else
	{
		yPos = ((int)y / (int)blockSizeH);
		xPos = ((int)x / (int)blockSizeW);
		gridPosition = (int)(yPos * MAXSQUARESSCREEN)+ xPos;
	}
	return gridPosition;
}


//======================================================================================
// FUNCTION     : GetEnemyShipX
// DESCRIPTION  : Get XC EnemyShip position
// PARAMETERS   : void
// RETURNS      : float returnCode			: return the new X position to EnemyShip.
//======================================================================================
float Grid::GetEnemyShipX(int pos, float xEnemyShip)
{
	float gridPosition = 0.0;
	float enemyWidth = ENEMYSHIP_DISPLAY_WIN_WIDTH;
	float winWidth = WIN_WIDTH;
	
	
	float diff = (1 - (enemyWidth / winWidth)) + 1;

	gridPosition = xEnemyShip * diff;

	
	return gridPosition;
}


int Grid::GetWindowSizeH()
{
	return windowSizeH;
}

int Grid::GetWindowSizeW()
{
	return windowSizeW;
}

int Grid::GetRow(float yValue)
{
	int feedback = 0; 
	float blockSizeH = GetWindowSizeH() / MAXSQUARESSCREEN;

	feedback = (int)(yValue / blockSizeH);

	return feedback;
}

float Grid::GetWidthBlock(int pos)
{
	return myGrid[pos].width;
}

float Grid::GetHeightBlock(int pos)
{
	return myGrid[pos].height;
}

float Grid::GetBlockSizeH(void)
{
	return (float)GetWindowSizeH() / MAXSQUARESSCREEN;
}

float Grid::GetBlockSizeW(void)
{
	return (float)GetWindowSizeW() / MAXSQUARESSCREEN;
}

float Grid::GetX(int pos)
{
	return myGrid[pos].x;
}

float Grid::GetY(int pos)
{
	return myGrid[pos].y;
}

float Grid::GetYC(int pos)
{
	return myGrid[pos].yc;
}

float Grid::GetXC(int pos)
{
	return myGrid[pos].xc;
}

void Grid::SetWindowSizeW(int newValue)
{
	windowSizeW = newValue;
}

void Grid::SetWindowSizeH(int newValue)
{
	windowSizeH = newValue;
}

void Grid::SetX(int pos, float theValue)
{
	myGrid[pos].x = theValue;
}

void Grid::SetY(int pos, float theValue)
{
	myGrid[pos].y = theValue;
}

void Grid::SetXC(int pos, float theValue)
{
	myGrid[pos].xc = theValue;
}

void Grid::SetYC(int pos, float theValue)
{
	myGrid[pos].yc = theValue;
}

void Grid::SetWidth(int pos, float theValue)
{
	myGrid[pos].width = theValue;
}

void Grid::SetHeight(int pos, float theValue)
{
	myGrid[pos].height = theValue;
}
