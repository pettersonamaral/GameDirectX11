/***********************************************************************************************************************
* FILE : 		GameLevel.h
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Apr-12

* DESCRIPTION/Requirements:
	This class will keep the GameLevel information/definition.
	Holds the GameLevel functions/methods prototypes, and member/attributes declaration.

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View

************************************************************************************************************************/
#pragma once

#include "Graphics.h"
#include "SpriteSheet.h"


class GameLevel
{
protected:
	static Graphics* gfx;

public:
	static void Init(Graphics* graphics)
	{
		gfx = graphics;
	}

	virtual void Load() = 0;
	virtual void Unload() = 0;
	virtual void Update() = 0;
	virtual void Render() = 0;
};