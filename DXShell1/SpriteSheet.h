/***********************************************************************************************************************
* FILE : 		SpriteSheet.h
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Apr-12

Introduction
	Holds the SpriteSheet functions/methods prototypes, and member/attributes declaration.
	SpriteSheet Class definition.

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
************************************************************************************************************************/

#pragma once
#pragma comment(lib, "dxguid")

#include <wincodec.h> //This is the WIC codec header - we need this to decode image files
#include "Graphics.h" //This includes both Windows and D2D libraries
#include "Grid.h"
#include <d2d1_1.h>
#include <d2d1_1helper.h>
#include <d3d11_1.h>
#include <d2d1effects.h>
#include <d2d1effecthelpers.h>
#include <dwrite_1.h>
#include <d2d1effects_2.h>
#include <wrl.h>
//Remember to add "windowscodecs.lib" to your Linker/Input/AdditionalDependencies

using namespace Microsoft::WRL;


class SpriteSheet
{
	Graphics* gfx; //Reference to the Graphics class
	ID2D1Bitmap* bmp; //This will hold our loaded and converted Bitmap file
	ComPtr<ID2D1Effect> chromakeyEffect;
	ComPtr<ID2D1Effect> scaleEffect;
	ComPtr<ID2D1Effect> rotateEffect;



public:
	//Constructor
	SpriteSheet(wchar_t* filename, Graphics* gfx);

	//Destructor
	~SpriteSheet();

	//Draw bitmap to the render target
	void Draw();

	void DrawEnemyShip(float x, float y);
	void DrawPlanet(int x);
	void ChromaPlanet(int planetPosition);
	void ChromaExplosionDSat(int gradePosition);
	void ChromaDSat(int gradePosition, float angle);
};