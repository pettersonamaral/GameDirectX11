/***********************************************************************************************************************
* FILE : 		Graphics.h
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Apr-12

* DESCRIPTION/Requirements:
	This class will keep the Graphics information/definition.
	Holds the Graphics functions/methods prototypes, and member/attributes declaration.

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View

// https://katyscode.wordpress.com/2013/01/23/migrating-existing-direct2d-applications-to-use-direct2d-1-1-functionality-in-windows-7/
// https://msdn.microsoft.com/en-us/library/windows/desktop/dd371461(v=vs.85).aspx
************************************************************************************************************************/
#pragma once
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "Dwrite.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "windowscodecs.lib")
#pragma comment(lib, "d2d1.lib")

#include <Windows.h>
#include <iostream>
#include <d2d1_1.h>
#include <d2d1_1helper.h>
#include <d3d11_1.h>
#include <d2d1effects.h>
#include <d2d1effects_2.h>
#include <d2d1effecthelpers.h>
#include <dwrite_1.h>
#include <cmath>
#include "Grid.h"


class Graphics
{
	//Below, these are all COM interfaces we're using to create D2D resources.
	//We release them as part of the ~Graphics deconstructor... or bad things can happen
	//ID2D1Factory* factory; //The factory allows us to create many other types of D2D resources
	
	ID2D1HwndRenderTarget* rendertarget; 

	// The render target device context https://msdn.microsoft.com/en-us/library/windows/desktop/hh404479(v=vs.85).aspx
	ID2D1DeviceContext *Screen;

	ID2D1SolidColorBrush* brush; //Note this COM interface! Remember to release it!
	ID2D1GeometrySink* pSink;//retrieves a sink, and uses it to define an hourglass shape.
	ID2D1PathGeometry* m_pPathGeometry;
	HRESULT hr = NULL;
	ID3D11Device1* Direct3DDevice;	// Direct3D device
	ID3D11DeviceContext1* Direct3DContext;// Direct3D device context
	ID2D1Device* Direct2DDevice;// Direct2D device
	ID2D1Bitmap1 *Direct2DBackBuffer; // Direct2D target rendering bitmap  // (linked to DXGI back buffer which is linked to Direct3D pipeline)
	ID2D1Factory1 *Direct2D;// Direct2D factory access
	IDXGISwapChain1 *DXGISwapChain;// DXGI swap chain




public:
	Graphics();

	~Graphics();

	bool Init(HWND windowHandle);

	ID2D1RenderTarget* GetRenderTarget()
	{
		return rendertarget;
	}

	ID2D1DeviceContext* GetDeviceContext()
	{
		return Screen;

		//I redo my factory, so it doesn't work for me. 
		//ID2D1DeviceContext *rtDC;
		//rendertarget->QueryInterface(&rtDC);
		//return rtDC;
	}	

	void BeginDraw() { Screen->BeginDraw(); }
	
	void EndDraw() { Screen->EndDraw(); 
		// Present (new for Direct2D 1.1)
		DXGI_PRESENT_PARAMETERS parameters = { 0 };
		parameters.DirtyRectsCount = 0;
		parameters.pDirtyRects = nullptr;
		parameters.pScrollRect = nullptr;
		parameters.pScrollOffset = nullptr;

		hr = DXGISwapChain->Present1(1, 0, &parameters);
	}

	void ClearScreen(float r, float g, float b);
};
