/***********************************************************************************************************************
* FILE : 		Level1.h
* PROJECT : 	GAS_AS03 --SET Invaders-- PROG2215 - Graphics, Animation and Sound
* PROGRAMMER : 	Petterson Amaral
* FIRST VERSION:2018-Apr-12

Introduction
	Holds the Level1 functions/methods prototypes, and member/attributes declaration.
	Level1 class definition.

Source: this code is base on https://conestoga.desire2learn.com/d2l/le/content/185141/viewContent/3999955/View
************************************************************************************************************************/

#pragma once
#pragma comment(lib, "dxguid")
#include "GameLevel.h"
#include <vector>
#include <math.h>
#include <Mmsystem.h>



class Level1 : public GameLevel
{
	float y;
	float x;
	float enemyShipSpeed;
	float lastYaxisValue;
	float yEnemyShip;
	float xEnemyShip;
	float ySpeed;
	float xSpeed;
	float durationCollision;
	int enemyEffectNew;
	int delayAssetEnemyShip;	
	int positionCollision;
	bool isDetectedCollision;
	bool enemyAssetToggleDirection;
	bool enemyShipMoveDirection;
	SpriteSheet* Background01;
	SpriteSheet* Planet01;
	SpriteSheet* Planet02;
	SpriteSheet* Planet03;
	SpriteSheet* EnemyAsset01;
	SpriteSheet* EnemyAsset02;
	SpriteSheet* EnemyAsset03;
	SpriteSheet* EnemyAsset04;
	SpriteSheet* DSat;
	SpriteSheet* ExplosionDSat;
	time_t seconds;
	std::vector <int> planetsPositions;
	struct sDSat
	{
		float rotateAngleDSat;
		int gridPositionDSat;
		bool isDSatLive;
	} typedef sDSat;

	std::vector <sDSat> myDSat;
	std::vector <SpriteSheet*> EnemyShip;

public:
	void Load()   override;
	void Unload() override;
	void Update() override;
	void Render() override;
	void SetPlanetsPositions(int qtt);
	void SetDSatPosition(int qtt);
	void InitPlanets(int qtt);
	void SetDSatRotation(int qtt);
	void SetEnemyShipToggleAsset(void);
	void SetEnemyShipNewPosition(void);
	void InitDSats(int qtt);
	bool GetDSatCollision(int qtt);
	int GetRandomGeneration(int limit);
};
